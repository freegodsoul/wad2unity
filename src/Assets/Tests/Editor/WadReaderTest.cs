﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using DoomWad;

public class WadReaderTest : MonoBehaviour
{
	public string textureName = "BSTONE1";
	public string patchName = "WALL00_1";
	private string wadname = "Assets/WAD/doom2.wad";

	// Use this for initialization
	void Start()
	{
		WadReader wad = new WadReader( wadname );
		if ( !wad.isWad )
			Debug.LogError( "wrong" );
		
		// Patch
		//Texture patchtex = reader.LoadTexture_Patch( wadname, patchName );
		//UnityUtils.SaveTextureToFile( patchtex, "png/" + patchName + ".png" );
		//AssetDatabase.Refresh();

		// Texture List
		maptexture_t[] textures = wad.maptextures;
		//Log.WriteLine( "Map textures:" );
		//foreach ( maptexture_t mt in textures )
		//	Log.WriteLine( "  " + mt.name );

		// Tex
		Texture walltex = wad.CreateTexture_Wall( textureName, FilterMode.Point );
		Debug.Log( walltex );
		AssetUtils.SaveTextureToFile( walltex, "png/" + textureName + ".png" );
		AssetDatabase.Refresh();
	}

	// Update is called once per frame
	void Update()
	{
	}
}