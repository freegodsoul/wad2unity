﻿using UnityEngine;
using System.Collections.Generic;
using System;
using DoomWad;


public class DoomMapTriangulationTest : MonoBehaviour
{
	public string mapname = "MAP01";
	public float scale = 1f / 256f;
	public bool showTriangles = true;
	public int selectedTriangle = 0;

	private InputController input;
	private WadMapImporter importer;
	private WadMapData map;
	private Material mat;

	void Awake()
	{
		input = GameObject.FindObjectOfType<InputController>();
		importer = new WadMapImporter();
		importer.scale = scale;
		importer.Import( "Assets/WAD/doom2.wad", mapname );
		map = importer.map;

		Shader shader = Shader.Find( "Custom/Debug" );
		mat = new Material( shader );
	}

	// Update is called once per frame
	void Update()
	{
		if ( Input.GetKeyDown( KeyCode.T ) )
			showTriangles = !showTriangles;
	}

	void OnDrawGizmos()
	{
		if ( !Application.isPlaying )
			return;

		Vector3 vScale = scale * Vector3.one;
		Gizmos.matrix = Matrix4x4.Scale( vScale );

		// Ребра

		Gizmos.color = Color.gray;
		for ( int i = 0; i < map.linedefs.Length; i++ )
		{
			maplinedef_t ld = map.linedefs[ i ];
			map.DrawSegment( ld.v1, ld.v2, 10f );
		}

		// Вершины

		for ( int i = 0; i < map.onlyLinedefVertexCount; i++ )
		{
			map.DrawVertex( i, 4f );
		}

		if ( showTriangles )
		{
			//foreach ( Poly2Tri.DelaunayTriangle t in importer.pointSet.Triangles )
			//{
			//	Vector2[] triangle = new Vector2[ 3 ];

			//	triangle[ 0 ] = new Vector2( ( float ) t.Points[ 0 ].X, ( float ) t.Points[ 0 ].Y );
			//	triangle[ 1 ] = new Vector2( ( float ) t.Points[ 1 ].X, ( float ) t.Points[ 1 ].Y );
			//	triangle[ 2 ] = new Vector2( ( float ) t.Points[ 2 ].X, ( float ) t.Points[ 2 ].Y );

			//	//foreach ( Vector2 p in triangle )
			//	//	Debug.Log( p );

			//	DebugDrawUtils.DrawWirePolygon( triangle, AxisPlane.XY, 0.1f );
			//}

			foreach ( MapTriangle t in importer.triangles )
			{
				Vector2[] triangle = new Vector2[ 3 ];
				Gizmos.color = t.sector != -1 ? new Color( 0f, 1f, 0f, 0.1f ) : new Color( 1f, 0f, 0f, 0.1f );

				triangle[ 0 ] = ( Vector2 ) map.vertices[ t.v1 ];
				triangle[ 1 ] = ( Vector2 ) map.vertices[ t.v2 ];
				triangle[ 2 ] = ( Vector2 ) map.vertices[ t.v3 ];

				DebugDrawUtils.DrawWirePolygon( triangle, AxisPlane.XY, 2f );
			}
		}
	}

	void OnRenderObject()
	{
		if ( showTriangles )
		{
			Vector3 vScale = scale * Vector3.one;

			GL.PushMatrix();
			GL.LoadProjectionMatrix( Camera.current.projectionMatrix );
			GL.MultMatrix( Camera.current.worldToCameraMatrix );
			GL.MultMatrix( Matrix4x4.Scale( vScale ) );

			// Greens (Accepted)

			mat.color = new Color( 0f, 1f, 0f, 0.1f );
			mat.SetPass( 0 );

			GL.Begin( GL.TRIANGLES );
			foreach ( MapTriangle t in importer.triangles )
				if ( t.sector != -1 )
					DrawTriangle( ( Vector2 ) map.vertices[ t.v1 ], ( Vector2 ) map.vertices[ t.v3 ], ( Vector2 ) map.vertices[ t.v2 ] );
			GL.End();

			// Reds (Culled)

			mat.color = new Color( 1f, 0f, 0f, 0.1f );
			mat.SetPass( 0 );

			GL.Begin( GL.TRIANGLES );
			foreach ( MapTriangle t in importer.triangles )
				if ( t.sector == -1 )
					DrawTriangle( ( Vector2 ) map.vertices[ t.v1 ], ( Vector2 ) map.vertices[ t.v3 ], ( Vector2 ) map.vertices[ t.v2 ] );
			GL.End();

			// Yellow (Selected)

			if ( selectedTriangle != -1 )
			{
				MapTriangle t = importer.triangles[ selectedTriangle ];

				mat.color = new Color( 1f, 1f, 0f, 0.1f );
				mat.SetPass( 0 );

				GL.Begin( GL.TRIANGLES );
				DrawTriangle( ( Vector2 ) map.vertices[ t.v1 ], ( Vector2 ) map.vertices[ t.v3 ], ( Vector2 ) map.vertices[ t.v2 ] );
				GL.End();
			}

			GL.PopMatrix();
		}
	}

	private void DrawTriangle( Vector2 v1, Vector2 v2, Vector2 v3 )
	{
		GL.Vertex3( v1.x, v1.y, 0f );
		GL.Vertex3( v2.x, v2.y, 0f );
		GL.Vertex3( v3.x, v3.y, 0f );
	}
}