﻿using UnityEngine;
using System.Collections.Generic;

public class ConvexPolyTest : MonoBehaviour
{
	public ConvexPoly poly = new ConvexPoly();
	public float gridSize = 0.1f;
	public bool leftSide = false;
	
	private Vector2 worldMousePosition;
	private bool bisectMode = false;
	private Vector2 bisectStartPoint;

	// Use this for initialization
	void Start()
	{
		worldMousePosition = UnityUtils.ScreenPointToWorldXY( Input.mousePosition );
		worldMousePosition = MathUtils.SnapPointToGrid( worldMousePosition, gridSize );
	}

	// Update is called once per frame
	void Update()
	{
		worldMousePosition = UnityUtils.ScreenPointToWorldXY( Input.mousePosition );
		worldMousePosition = MathUtils.SnapPointToGrid( worldMousePosition, gridSize );
		
		if ( bisectMode )
		{
			if ( Input.GetKeyUp( KeyCode.Mouse1 ) )
			{
				bisectMode = false;
				poly.Bisect( bisectStartPoint, worldMousePosition, leftSide );
			}
		}
		else
		{
			if ( Input.GetKey( KeyCode.Mouse0 ) )
			{
				poly.AddPoint( worldMousePosition );
			}

			if ( Input.GetKeyDown( KeyCode.Mouse1 ) )
			{
				bisectMode = true;
				bisectStartPoint = worldMousePosition;
			}

			// Triangulate
			if ( Input.GetKeyDown( KeyCode.Return ) )
			{
				Mesh mesh = MeshUtils.CreateSolidConvexPolygon( poly.points.ToArray(), Color.white, AxisPlane.XY );
				mesh.name = "ConvexPoly";
				GameObject go = new GameObject( "Triangulated" );
				go.SetMeshToGameObject( mesh, null );
			}
		}
	}

	void OnDrawGizmos()
	{
		if ( !Application.isPlaying )
			return;

		Gizmos.matrix = Matrix4x4.identity;
		Gizmos.color = Color.white;
		Gizmos.DrawWireCube( worldMousePosition, Vector3.one * 0.2f );

		Gizmos.color = Color.red;
		DebugDrawUtils.DrawWirePolygon( poly.points.ToArray(), AxisPlane.XY, 0.1f );

		if ( bisectMode )
		{
			Vector2 additor = ( worldMousePosition - bisectStartPoint ) * 1000f;
			Gizmos.color = Color.red;
			Gizmos.DrawLine( bisectStartPoint - additor, worldMousePosition + additor );
		}
	}
}