﻿using UnityEngine;
using System.Collections.Generic;
using DoomWad;

public class DoomMapTest : MonoBehaviour
{
	public string mapname = "MAP01";
	public float scale = 1f / 256f;
	public int selectedSector = 0;
	public int selectedContour = 0;
	public int selectedSubSector = 0;
	public int selectedNode = 0;
	public ConvexPoly debugPoly;

	private WadMapImporter importer;
	private WadMapData map;

	private int processedSector = 0;
	private int processedSubSector = 0;


	// Use this for initialization
	void Awake()
	{
		importer = new WadMapImporter();
		importer.Import( "Assets/WAD/doom2.wad", mapname );
		map = importer.map;
		processedSector = selectedSector;
		processedSubSector = selectedSubSector;
		selectedNode = map.nodes.Length - 1;

		// Инициализируем начальный полигон двумя bounding-box-ами левого и правого корневых узла
		//mapnode_t currentNode = map.nodes[ selectedNode ];
		//poly = new ConvexPoly();
		//poly.AddRect( currentNode.rbox );
		//poly.AddRect( currentNode.lbox );
	}

	// Update is called once per frame
	void Update()
	{
		if ( processedSector != selectedSector )
		{
			//importer.BuildSector( selectedSector );
			processedSector = selectedSector;
		}

		if ( processedSubSector != selectedSubSector )
		{
			selectedSector = map.FindSectorBySubSector( selectedSubSector );
			processedSubSector = selectedSubSector;
		}

		// INPUT

		if ( Input.GetKeyDown( KeyCode.L ) )
		{
			//selectedNode = importer.ClipPolygon( poly, selectedNode, false );
			mapnode_t currentNode = map.nodes[ selectedNode ];
			selectedNode = currentNode.lchild;
		}

		if ( Input.GetKeyDown( KeyCode.R ) )
		{
			//selectedNode = importer.ClipPolygon( poly, selectedNode, true );
			mapnode_t currentNode = map.nodes[ selectedNode ];
			selectedNode = currentNode.rchild;
		}

		if ( Input.GetKeyDown( KeyCode.Q ) )
		{
			selectedNode = map.nodes.Length - 1;
		}

		// NODE/SUBSECTOR/POLY POST PROCESSING

		if ( selectedNode != -1 )
		{
			if ( selectedNode < 0 )
			{
				selectedSubSector = selectedNode + ( int ) NodeFlags.Leaf;
			}
		}
	}

	void OnDrawGizmos()
	{
		if ( !Application.isPlaying )
			return;

		Vector3 vScale = scale * Vector3.one;
		Gizmos.matrix = Matrix4x4.Scale( vScale );

		// Ребра

		Gizmos.color = Color.gray;
		for ( int i = 0; i < map.linedefs.Length; i++ )
		{
			maplinedef_t ld = map.linedefs[ i ];
			map.DrawSegment( ld.v1, ld.v2, 10f );
		}

		// Вершины

		for ( int i = 0; i < map.onlyLinedefVertexCount; i++ )
		{
			map.DrawVertex( i, 4f );
		}
		
		// Ребра выбранного сектора

		if ( selectedSector >= 0 && selectedSector < map.sectors.Length )
		{
			Gizmos.color = Color.magenta;
			for ( int i = 0; i < map.linedefs.Length; i++ )
			{
				maplinedef_t ld = map.linedefs[ i ];

				if ( ( ld.lsidenum != -1 && map.sidedefs[ ld.lsidenum ].sector == selectedSector ) ||
					 ( ld.rsidenum != -1 && map.sidedefs[ ld.rsidenum ].sector == selectedSector ) )
				{
					map.DrawSegment( ld.v1, ld.v2, 10f );
				}
			}
		}

		// Ребра one-sided выбранного сектора

		//Gizmos.color = Color.yellow;
		//foreach ( int i in importer.oneSidedSectorLinedefs )
		//{
		//	maplinedef_t ld = map.linedefs[ i ];
		//	DrawSegment( ld.v1, ld.v2 );
		//}

		// Ребра выбранного контура

		//if ( selectedContour >= 0 && selectedContour < importer.contours.Count )
		//{
		//	const float fade = 0.75f;

		//	// Контур целиком
		//	Gizmos.color = Color.green;
		//	List<int> contour = importer.contours[ selectedContour ];
		//	foreach ( int i in contour )
		//	{
		//		maplinedef_t ld = map.linedefs[ i ];
		//		DrawSegment( ld.v1, ld.v2 );
				
		//		Gizmos.color = new Color(
		//			Gizmos.color.r * fade,
		//			Gizmos.color.g * fade,
		//			Gizmos.color.b * fade, 1f );
		//	}
		//}

		// Ребра субсектора

		if ( selectedSubSector >= 0 && selectedSubSector < map.subsectors.Length )
		{
			mapsubsector_t ss = map.subsectors[ selectedSubSector ];

			Gizmos.color = Color.yellow;
			for ( int i = 0; i < ss.numsegs; i++ )
			{
				mapseg_t seg = map.segs[ ss.firstseg + i ];
				map.DrawSegment( seg.v1, seg.v2, 10f );
			}
		}

		// Node

		//if ( selectedNode >= 0 && selectedSubSector < map.nodes.Length )
		//{
		//	mapnode_t node = map.nodes[ selectedNode ];

		//	Vector3 v1 = new Vector3( node.x, node.y );
		//	Vector3 v2 = new Vector3( node.x + node.dx, node.y + node.dy );

		//	Gizmos.color = Color.red;
		//	DebugDrawUtils.DrawWireRect( MathUtils.GetTaperRect( node.lbox, 2f ), AxisPlane.XY );

		//	Gizmos.color = Color.green;
		//	DebugDrawUtils.DrawWireRect( MathUtils.GetTaperRect( node.rbox, 2f ), AxisPlane.XY );

		//	Gizmos.color = Color.blue;
		//	Gizmos.DrawLine( v1, v2 );
		//}

		// Convex Poly

		if ( debugPoly != null )
		{	
			Gizmos.color = Color.blue;
			DebugDrawUtils.DrawWirePolygon( debugPoly.points.ToArray(), AxisPlane.XY, 10f );
		}
	}
}