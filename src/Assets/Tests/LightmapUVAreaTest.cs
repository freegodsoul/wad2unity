﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class LightmapUVAreaTest : MonoBehaviour
{
	public GameObject uvReference;

	public Vector2 pos = Vector2.zero;
	public Vector2 size = Vector2.one;

	private MeshFilter mf;

	// Update is called once per frame
	void Update()
	{
		mf = GetComponent<MeshFilter>();
		mf.sharedMesh = GenerateMesh();
	}

	private Mesh GenerateMesh()
	{
		Mesh mesh = new Mesh();
		mesh.name = "GeneratedQuad";
		
		Vector3[] vertices = new Vector3[ 4 ];
		vertices[ 0 ] = ( Vector3 ) pos;
		vertices[ 1 ] = ( Vector3 ) pos + Vector3.up * size.y;
		vertices[ 2 ] = ( Vector3 ) pos + ( Vector3 ) size;
		vertices[ 3 ] = ( Vector3 ) pos + Vector3.right * size.x;
		mesh.vertices = vertices;

		int[] triangles = new int[ 2 * 3 ];
		triangles[ 0 ] = 0;
		triangles[ 1 ] = 1;
		triangles[ 2 ] = 2;
		triangles[ 3 ] = 2;
		triangles[ 4 ] = 3;
		triangles[ 5 ] = 0;
		mesh.triangles = triangles;

		Vector2[] uv = new Vector2[ 4 ];
		uv[ 0 ] = new Vector2( 0f, 0f );
		uv[ 1 ] = new Vector2( 0f, 1f );
		uv[ 2 ] = new Vector2( 1f, 1f );
		uv[ 3 ] = new Vector2( 1f, 0f );
		mesh.uv = uv;
		
		if ( uvReference )
			mesh.uv2 = GenerateUV2( mesh );

		mesh.RecalculateNormals();
		mesh.RecalculateBounds();

		return mesh;
	}
	
	private Vector2[] GenerateUV2( Mesh mesh )
	{
		Vector2[] uv2 = new Vector2[ mesh.vertexCount ];
		for ( int i = 0; i < mesh.vertexCount; i++ )
		{
			Vector3 v = transform.localToWorldMatrix * mesh.vertices[ i ];
			uv2[ i ] = CalcVertexUV( v );
		}
		return uv2;
	}

	private Vector2 CalcVertexUV( Vector3 vertex )
	{
		float w = uvReference.collider2D.bounds.extents.x * 2f;
		float h = uvReference.collider2D.bounds.extents.y * 2f;

		float u = ( vertex.x - uvReference.collider2D.bounds.min.x ) / w;
		float v = ( vertex.y - uvReference.collider2D.bounds.min.y ) / h;

		return new Vector2( u, v );
	}
}