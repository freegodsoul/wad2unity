﻿using UnityEngine;
using System.Collections;

namespace DoomWad
{
	public class MapWall : MapObject
	{
		public LineDefFlags flags;
		public MapSide[] lsides = new MapSide[ ( int ) SideType.MAX ];
		public MapSide[] rsides = new MapSide[ ( int ) SideType.MAX ];

		protected new void Awake()
		{
			base.Awake();
		}

		// Update is called once per frame
		protected new void Update()
		{
			base.Update();
		}
	}
}