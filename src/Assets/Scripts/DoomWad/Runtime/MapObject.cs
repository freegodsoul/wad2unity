﻿using UnityEngine;
using System.Collections;

public class MapObject : MonoBehaviour
{
	// direct link to mesh
	[HideInInspector]
	public Mesh mesh;

	// direct link to material
	[HideInInspector]
	public Material mat;

	// Use this for initialization
	protected void Awake()
	{
	}
	
	// Update is called once per frame
	protected void Update()
	{
	}
}
