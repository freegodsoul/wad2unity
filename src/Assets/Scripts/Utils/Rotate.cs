﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour
{
	public float angularVelocity = 30f;
	public Vector3 axis = Vector3.up;

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{
		float angleDelta = angularVelocity * Time.deltaTime;
		transform.rotation =  Quaternion.AngleAxis( angleDelta, axis ) * transform.localRotation;
	}
}