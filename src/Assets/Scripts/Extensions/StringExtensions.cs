﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

public static class StringExtensions
{
	public static string Decrement( this string s )
	{
		return s.Substring( 0, s.Length - 1 );
	}

}